﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.ObjectModel;

namespace DataGridDemo
{
	public partial class MainWindowViewModel : ObservableObject
	{
		[ObservableProperty]
		private ObservableCollection<PersonModel> personLists;

        public MainWindowViewModel()
        {
            PersonLists = new ObservableCollection<PersonModel>
            {
                new()
                {
                    Id = 1,
                    Name = "潘中单",
                    Age = 24,
                    Gender = "男",
                    Description = "神一样的男人"
                },
				new()
				{
					Id = 2,
					Name = "刘世美",
					Age = 22,
					Gender = "女",
					Description = "一个美丽的女子"
				},
				new()
				{
					Id = 3,
					Name = "孟世成",
					Age = 25,
					Gender = "男",
					Description = "神一样的男人"
				},
				new()
				{
					Id = 4,
					Name = "楚留香",
					Age = 24,
					Gender = "男",
					Description = "神一样的男人"
				},
				new()
				{
					Id = 5,
					Name = "哮天犬",
					Age = 24,
					Gender = "男",
					Description = "神一样的男人"
				},
				new()
				{
					Id = 6,
					Name = "哮天犬",
					Age = 24,
					Gender = "男",
					Description = "神一样的男人"
				},
				new()
				{
					Id = 7,
					Name = "哮天犬",
					Age = 24,
					Gender = "男",
					Description = "神一样的男人"
				},
				new()
				{
					Id = 8,
					Name = "哮天犬",
					Age = 24,
					Gender = "男",
					Description = "神一样的男人"
				},
				new()
				{
					Id = 9,
					Name = "哮天犬",
					Age = 24,
					Gender = "男",
					Description = "神一样的男人"
				}
			};
        }
    }
}
