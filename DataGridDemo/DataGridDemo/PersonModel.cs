﻿namespace DataGridDemo
{
	public class PersonModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int Age { get; set; }
		public string Gender { get; set; }
		public string Description { get; set; }
	}
}
